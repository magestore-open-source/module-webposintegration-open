<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\WebposIntegration\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magestore\WebposIntegration\Api\ApiServiceInterface;

/**
 * Setup Integration Data
 */
class SetupIntegration implements DataPatchInterface
{
    /**
     * @var ApiServiceInterface
     */
    private $apiService;

    /**
     * @param ApiServiceInterface $apiService
     */
    public function __construct(
        ApiServiceInterface $apiService
    ) {
        $this->apiService = $apiService;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->apiService->setupIntegration();
        $integration = $this->apiService->getIntegration();
        $this->apiService->createAccessToken($integration);
        return $this;
    }
}
